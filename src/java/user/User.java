package user;

public abstract class User {

    private String user;
    private  String pass;
    private String name;
    
    

    public User(String user, String pass) {
        this.user = user;
        this.pass = pass;
    }
    
    
    void register() {
        System.out.println("Registro general"); 
    }

    void loging() {
         System.out.println("loging general"); 
    }

    void logout() {
         System.out.println("logout general"); 
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
 
}
